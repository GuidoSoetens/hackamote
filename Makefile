# Attempt to load a config.make file.
# If none is found, project defaults in config.project.make will be used.
ifneq ($(wildcard config.make),)
	include config.make
endif

# Attempt to load a config.make.local file
ifneq ($(wildcard config.make.local),)
	include config.make.local
endif

OF_ROOT=$(FRAMEWORK_ROOT)
# make sure the the OF_ROOT location is defined
ifeq ($(OF_ROOT),)
	OF_ROOT=../../..
endif

# call the project makefile!
include $(OF_ROOT)/libs/openFrameworksCompiled/project/makefileCommon/compile.project.mk

.PHONY: test it
test:
	make -j4 -C test/ && GTEST_COLOR=1 ./test/tests
it:
	make -j4 Debug 2>&1 | grep -E 'error|undefined|warning' || make RunDebug
