#version 130

precision highp float;

uniform sampler2DRect uTexture;
uniform vec2 uResolution;

varying vec2 vTexPos;
varying vec3 vWaterPos;

void main() {
    vec2 uv = mod(vTexPos, 1.0);
    gl_FragColor = texture2DRect(uTexture, uv * uResolution);

    float depth = clamp(vWaterPos.z / 10000.0 - 1.0, 0., .8);
    vec4 mixClr = vec4(0.302, 0.6902, 0.8745, 1.0);
    gl_FragColor = mix(gl_FragColor, mixClr, depth);

    // float bFrac = .01;
    // if(uv.x < bFrac || uv.x > (1. - bFrac) || uv.y < bFrac || uv.y > (1. - bFrac))
    //     gl_FragColor.rgb = vec3(1.);
}