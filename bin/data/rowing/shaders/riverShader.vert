#version 130

varying vec2 vTexPos;
varying vec3 vWaterPos;

void main() {
    gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vTexPos = gl_TexCoord[0].xy;
    vWaterPos = gl_Position.xyz;
}