#pragma once

const static int SCREEN_WIDTH = 1280;
const static int SCREEN_HEIGHT = 800;


typedef enum {
    GameKey_MainMenu = 0,
    GameKey_SafariMap,
    GameKey_Rowing,
    GameKey_Photographs,
    GameKey_Fishing,
    GameKey_Silhouette
} GameKey;

class StartGameListener {
    public: 
        virtual void startGame(GameKey key) = 0;
};
