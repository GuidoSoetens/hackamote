#include "GameWindow.h"
#include "Defs.h"
#include "InputManager.h"
#include "RowGame.h"
#include "SafariGame.h"
#include "SafariMap.h"
#include "MainMenu.h"
#include "Fishing.h"
#include "SilhouetteGame.h"

GameWindow::GameWindow()
:   mStateText("Dit is de status:")
,   mGameReference(NULL)
{
    
}

GameWindow::~GameWindow()
{
    if(mGameReference != NULL)
        delete mGameReference;
}

void GameWindow::setup() {
    mFont.loadFont("fontje.otf", 18);
    startMenu();
}

void GameWindow::update() {

    float dt = fmaxf(0.00001, ofGetLastFrameTime());
    InputManager::Instance()->update(dt);
    if(mGameReference != NULL)
        mGameReference->update(dt);

    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 1) {
        if(wiimotes[0]->isHomeButtonPressed()) {
            if(mGameReference != NULL) {
                if(mGameReference->isGame())
                    startMenu();
            }
        }
    }
}

void GameWindow::draw() {
    ofSetColor(255);
    if(mGameReference != NULL)
        mGameReference->draw();
}

void GameWindow::startGame(Game* game) {
    if(mGameReference != NULL)
        delete mGameReference;
    mGameReference = game;
    mGameReference->setup();
}

void GameWindow::startGame(GameKey key) {
    switch(key) {
        case GameKey_SafariMap:
            startGame(new SafariMap(this));
            break;
        case GameKey_Rowing:
            startGame(new RowGame());
            break;
        case GameKey_Photographs:
            startGame(new SafariGame());
            break;
        case GameKey_Fishing:
            startGame(new Fishing());
            break;
        case GameKey_Silhouette:
            startGame(new SilhouetteGame());
            break;
        case GameKey_MainMenu:
        default:
            startGame(new MainMenu(this));
            break;
    }
}

void GameWindow::startMenu() {
    startGame(GameKey_MainMenu);
}

void GameWindow::keyPressed(int key) {
    if(key == 'b' || key == 'B' || key == 'm' || key == 'M') {
        startMenu();
    }
}
