/* Copyright (C) Active Cues - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  * 
  * Original author Guido Soetens <guido@activecues.com>, June 2019
*/

#include "ofMain.h"
#include "GameWindow.h"
#include "Defs.h"

//========================================================================
int main ( int argc, char *argv[] )
{
    ofSetupOpenGL(SCREEN_WIDTH,SCREEN_HEIGHT,OF_WINDOW);//OF_FULLSCREEN);

    try {
	    ofRunApp(new GameWindow());
    } catch (...) {
        ofExit(1);
        return 1;
    }

    ofExit(0);
    return 0;
}
