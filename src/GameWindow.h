/* Copyright (C) Active Cues - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  * 
  * Original author Guido Soetens <guido@activecues.com>, June 2019
*/

#pragma once

#include "ofMain.h"
#include "Game.h"
#include "Defs.h"

class GameWindow : public ofBaseApp, StartGameListener {

public:
    GameWindow();
    ~GameWindow();

	void setup();
	void update();
	void draw();

  void keyPressed(int key);

  void startGame(GameKey key);

private:

    string mStateText;
    ofTrueTypeFont mFont;

    Game* mGameReference;

    void startGame(Game* game);
    void startMenu();
};
