#pragma once

#include "Game.h"
#include "Defs.h"

class MainMenu : public Game {
public:
    MainMenu(StartGameListener * gameListener);
    virtual ~MainMenu();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();

    virtual bool isGame() { return false; };

private:

    ofImage mCursorImage;
    vector<ofImage> mDoorImages;

    StartGameListener* mStartGameListener;
    ofVec2f mCursorPos;

    float mScrollTimer;
    int mCenterIndex;
    float mScrollOffset;
    int mSubsequentScrolls;
};
