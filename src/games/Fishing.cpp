#include "Fishing.h"
#include "Defs.h"
#include "InputManager.h"

Fishing::Fishing() 
:   mCursorPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
{

}

Fishing::~Fishing() {
    
}

void Fishing::setup() {
    mCursorImage.loadImage("mainmenu/img/cursor.png");
    mVideo.load("fishing/video/fishing.mp4");
    mVideo.play();
}

void Fishing::update(float dt) {
    mVideo.update();

    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 1) {
        ofVec2f size(SCREEN_WIDTH, SCREEN_HEIGHT);
        ofVec2f center = size / 2.0;
        mCursorPos = center + 1.5 * size * wiimotes[0]->getPosition();
    }
}

void Fishing::draw() {
    ofSetColor(255);
    mVideo.draw(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    ofSetColor(255);
    ofPushMatrix();
    ofTranslate(mCursorPos);
    ofScale(.5, .5);
    ofTranslate(-mCursorImage.getWidth() * .2, -mCursorImage.getHeight() * .1);
    mCursorImage.draw(0,0);
    ofPopMatrix();
}