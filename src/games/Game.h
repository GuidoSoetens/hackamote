#pragma once

#include "ofMain.h"

class Game {
public:
    Game() {};
    virtual ~Game() {};

    virtual void setup() = 0;
    virtual void update(float dt) = 0;
    virtual void draw() = 0;

    virtual bool isGame() { return true; };
};