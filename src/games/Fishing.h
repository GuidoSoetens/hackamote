#pragma once

#include "Game.h"

class Fishing : public Game {
public:
    Fishing();
    virtual ~Fishing();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();

private:

    ofVideoPlayer mVideo;

    ofImage mCursorImage;
    ofVec2f mCursorPos;
};