#include "RowGame.h"
#include "Defs.h"
#include "InputManager.h"

const static int RIVER_TILES = 1000;

const static float RIVER_UNIT_SIZE = 2.0 * SCREEN_WIDTH;

RowGame::RowGame() 
:   mBoatTurnFactor(0)
,   mBoatSpeed(0)
,   mBoatLocation(0,0)
,   mBoatAnimParam(0)
,   mRefreshTimer(0)
,   mUnitsTraversed(0)
,   mSplashSoundTimer(0)
{
    
} 

RowGame::~RowGame() {
    mBackgroundMusicId.unload();
    mRiverLoopId.unload();
    for(int i=0; i<mPeddleSoundIds.size(); ++i) {
        mPeddleSoundIds[i].unload();
    }
}

void RowGame::setup() {
    mBackgroundImage.loadImage("rowing/img/background.jpg");
    mBoatImage.loadImage("rowing/img/boat.png");
    mBushImage.loadImage("rowing/img/bush.png");
    mRockImage.loadImage("rowing/img/rock.png");
    mTreeImage.loadImage("rowing/img/tree.png");
    mWaterImage.loadImage("rowing/img/water.png");

    mRiverShader.load("rowing/shaders/riverShader");

    mBackgroundMusicId.load("rowing/sound/backgroundmusic.ogg");
    mBackgroundMusicId.setLoop(true);
    mRiverLoopId.load("rowing/sound/river_loop.ogg");
    mRiverLoopId.setLoop(true);
    for(int i=0; i<7; ++i) {
        ofSoundPlayer sp;
        sp.load("rowing/sound/peddle" + ofToString(i + 1) + ".ogg");
        mPeddleSoundIds.push_back(sp);
    }

    mBackgroundMusicId.play();
    mRiverLoopId.play();

    int numElems = 200;

    vector<int> trees = { 100, 125, 50, 170, 31 };

    for(int i=0; i<numElems; ++i) {
        float t = i / (float)(numElems - 1);

        bool left = i % 2 == 0;

        FoliageData f;
        f.position = ofVec2f((left ? -1 : 1) * RIVER_UNIT_SIZE * .75, t * RIVER_UNIT_SIZE);
        f.position.x += (ofRandom(2.0) - 1.0) * 100;
        f.imageRef = &mBushImage;
        f.scale = .8 * (.2 + ofRandom(.8));
        f.skipReps = false;
        f.flipImage = !left;
        f.groundOffset = .15;

        if(std::find(trees.begin(), trees.end(), i) != trees.end()) {
            f.imageRef = &mTreeImage;
            f.scale = 5.0;
            f.skipReps = true;
            f.groundOffset = 0.;
        }

        mFoliageElems.push_back(f);
    }
}

void RowGame::update(float dt) {

    mRefreshTimer = fmaxf(0, mRefreshTimer - dt);

    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 2) {
        bool left = wiimotes[0]->isMoving();
        bool right = wiimotes[1]->isMoving();

        float goalSpeed = mBoatSpeed;
        float goalTurnFactor = mBoatTurnFactor;

        if(wiimotes[0]->isBButtonPressed() || wiimotes[1]->isBButtonPressed()) {
            if(mRefreshTimer <= 0.0) {
                mRefreshTimer = 1.0;
                mRiverShader.load("shaders/riverShader");
            }
        }

        if(left && right) {
            goalSpeed = 1.0;
        }
        else if(left) {
            goalTurnFactor = -1.0;
            goalSpeed = 0.5;
        }
        else if(right) {
            goalTurnFactor = 1.0;
            goalSpeed = 0.5;
        }
        else {
            goalSpeed = 0.0;
            mSplashSoundTimer = 0;
        }

        if(left || right) {
            mSplashSoundTimer -= dt;
            if(mSplashSoundTimer < 0.0) {
                int n = mPeddleSoundIds.size();
                int idx = min(n - 1, (int)ofRandom(n));
                mPeddleSoundIds[idx].play();
                mSplashSoundTimer = 1.0;
            }
        }

        float frac = fminf(1.0, 1.0 * dt);
        mBoatSpeed = (1 - frac) * mBoatSpeed + frac * goalSpeed;

        float turnFrac = .3 * frac;
        mBoatTurnFactor = (1 - turnFrac) * mBoatTurnFactor + turnFrac * goalTurnFactor;
    }

    float angle = mBoatTurnFactor * .25 * M_PI + .5 * M_PI;
    mBoatLocation += dt * .75 * mBoatSpeed * ofVec2f(cosf(angle), sinf(angle));
    mBoatLocation.x = fmaxf(-1, fminf(1, mBoatLocation.x));

    int reps = 0;
    while(mBoatLocation.y > 1.0 && ++reps < 10) {
        mUnitsTraversed++;
        mBoatLocation.y = mBoatLocation.y - 1.0;
    }
    mUnitsTraversed = mUnitsTraversed % 5;

    float animSpeed = 5.0 - 3 * mBoatSpeed;
    mBoatAnimParam = fmodf(mBoatAnimParam + dt / animSpeed, 1.0);
}

void RowGame::draw() {

    ofCamera camera;
	camera.setupPerspective(true, 45, 0, 100000); 
    camera.begin();

    ofSetColor(255);
    mBackgroundImage.draw(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
    ofSetColor(72, 39, 30);
    ofDrawRectangle(0,SCREEN_HEIGHT/2,SCREEN_WIDTH,SCREEN_HEIGHT/2);

    ofMesh riverQuad;
    riverQuad.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    for(int i=0; i<4; ++i) {
        ofVec2f texPos(i / 2, i % 2);
        riverQuad.addVertex(ofVec2f((2 * texPos.x - 1) / 2.0 * RIVER_UNIT_SIZE, (2 * texPos.y - 1) * (RIVER_TILES / 2.0) * RIVER_UNIT_SIZE));
        riverQuad.addTexCoord(texPos * ofVec2f(1,RIVER_TILES));
    }


    ofVec2f boatPos = mBoatLocation;

    ofSetColor(255);
    ofPushMatrix();
    ofTranslate(0,SCREEN_HEIGHT/2);
    ofTranslate(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    ofRotate(mBoatTurnFactor * 45, 0, 1, 0);
    ofTranslate(mBoatLocation.x * .3 * RIVER_UNIT_SIZE, 0, mBoatLocation.y * RIVER_UNIT_SIZE);
    ofRotate(-90, 1, 0, 0);
    mRiverShader.begin();
    mRiverShader.setUniformTexture("uTexture", mWaterImage.getTextureReference(), 0);
    mRiverShader.setUniform2f("uResolution", mWaterImage.getWidth(), mWaterImage.getHeight());
    riverQuad.draw();
    mRiverShader.end();
    const int fol_reps = 30;
    for(int rep=0; rep<fol_reps; ++rep) {
        for(int i=0; i<mFoliageElems.size(); ++i) {

            if(mFoliageElems[i].skipReps) {
                int currIt = rep - mUnitsTraversed;
                if(currIt % 5 != 0)
                    continue;
            }

            float t = i / (float)mFoliageElems.size();

            ofVec2f pos = mFoliageElems[i].position;
            pos.y += (fol_reps - 1 - rep) * RIVER_UNIT_SIZE;
            ofPushMatrix();
            ofTranslate(pos);
            ofRotate(90,1,0,0);
            ofScale(mFoliageElems[i].scale, mFoliageElems[i].scale);
            if(mFoliageElems[i].flipImage)
                ofScale(-1, 1);
            ofTranslate(-mFoliageElems[i].imageRef->getWidth() / 2, -mFoliageElems[i].imageRef->getHeight() * (1 - mFoliageElems[i].groundOffset));
            mFoliageElems[i].imageRef->draw(0,0);
            ofPopMatrix();
        }
    }
    ofPopMatrix();

    //render boat:
    float boatHeight = .33 * SCREEN_HEIGHT;
    float boatScale = boatHeight / mBoatImage.getHeight();
    ofSetColor(255);
    ofPushMatrix();
    ofTranslate(SCREEN_WIDTH / 2, SCREEN_HEIGHT);
    ofTranslate(0, (.5 + .5 * sinf(mBoatAnimParam * 2 * M_PI)) * .25 * boatHeight);
    ofScale(boatScale, boatScale);
    ofTranslate(-mBoatImage.getWidth() / 2, -mBoatImage.getHeight());
    mBoatImage.draw(0,0);
    ofPopMatrix();

    camera.end();
}
