#pragma once

#include "Game.h"
#include "Defs.h"

class SafariMap : public Game {
public:
    SafariMap(StartGameListener * gameListener);
    virtual ~SafariMap();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();

private:
    float mKietelWobbleAnimParam;
    float mKietelTime;
    ofVec2f mGuidePos;
    void kietel();

    typedef struct {
        ofVec2f position;
        ofImage image;
        GameKey gameKey;
        bool selected;
        float focusScale;
    } GameOption;  
    vector<GameOption> mOptions;

    StartGameListener* mStartGameListener;
    ofVec2f mCursorPos;

    ofImage mCursorImage;

    ofImage mMapImage;
    ofImage mGuideImage;

    bool mAllowPress;

    ofSoundPlayer mWelcomeSound;
    ofSoundPlayer mKietelSound;
};