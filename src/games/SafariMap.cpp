#include "SafariMap.h"
#include "InputManager.h"

static const int NUM_OPTS = 6;

SafariMap::SafariMap(StartGameListener * gameListener) 
:   mCursorPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
,   mStartGameListener(gameListener)
,   mAllowPress(false)
,   mKietelTime(0)
,   mKietelWobbleAnimParam(1)
,   mGuidePos(.15 * SCREEN_WIDTH, .8 * SCREEN_HEIGHT)
{
    
}

SafariMap::~SafariMap() {
    mWelcomeSound.unload();
    mKietelSound.unload();
}

void SafariMap::setup() {
    mCursorImage.loadImage("safari_world/img/cursor.png");

    mMapImage.loadImage("safari_world/img/map.png");
    mGuideImage.loadImage("safari_world/img/safari_guide.png");

    GameOption opt;
    opt.position = ofVec2f(.35 * SCREEN_WIDTH,.65 * SCREEN_HEIGHT);
    opt.image.loadImage("safari_world/img/item_safari.png");
    opt.gameKey = GameKey_Photographs;
    mOptions.push_back(opt);

    opt.position = ofVec2f(.5 * SCREEN_WIDTH,.35 * SCREEN_HEIGHT);
    opt.image.loadImage("safari_world/img/item_fish.png");
    opt.gameKey = GameKey_Fishing;
    mOptions.push_back(opt);

    opt.position = ofVec2f(.7 * SCREEN_WIDTH,.6 * SCREEN_HEIGHT);
    opt.image.loadImage("safari_world/img/item_row.png");
    opt.gameKey = GameKey_Rowing;
    mOptions.push_back(opt);

    opt.position = ofVec2f(.25 * SCREEN_WIDTH,.5 * SCREEN_HEIGHT);
    opt.image.loadImage("safari_world/img/item_silhouette.png");
    opt.gameKey = GameKey_Silhouette;
    mOptions.push_back(opt);

    for(int i=0; i<mOptions.size(); ++i) {
        mOptions[i].selected = false;
        mOptions[i].focusScale = 1;
    }

    mWelcomeSound.load("safari_world/sound/welcome_safari.ogg");
    mKietelSound.load("safari_world/sound/kietel.ogg");

    mWelcomeSound.play();
}

void SafariMap::update(float dt) {

    mKietelTime = fmaxf(mKietelTime - dt, 0);
    mKietelWobbleAnimParam = fminf(mKietelWobbleAnimParam + dt, 1.0);

    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 1) {
        ofVec2f size(SCREEN_WIDTH, SCREEN_HEIGHT);
        ofVec2f center = size / 2.0;
        mCursorPos = center + 1.5 * size * wiimotes[0]->getPosition();
        
        if(wiimotes[0]->isAButtonPressed() || wiimotes[0]->isBButtonPressed()) {
            if(mStartGameListener != NULL && mAllowPress) {
                bool selected = false;
                for(int i=0; i<mOptions.size(); ++i) {
                    if(mOptions[i].selected) {
                        selected = true;
                        mStartGameListener->startGame(mOptions[i].gameKey);
                        break;
                    }
                }

                if(!selected && (mGuidePos - mCursorPos).length() < 200 && mKietelTime <= 0.0) {
                    kietel();
                }
            }
        }
        else {
            mAllowPress = true;
        }
    }

    float closestDist = (mOptions[0].position - mCursorPos).length();
    int closestIdx = 0;
    for(int i=1; i<mOptions.size(); ++i) {
        ofVec2f pos = mOptions[i].position;
        float dist = (pos - mCursorPos).length();
        if(dist < closestDist) {
            closestIdx = i;
            closestDist = dist;
        }
    }

    if(closestDist > 200) {
        closestIdx = -1;
    }

    for(int i=0; i<mOptions.size(); ++i) {
        mOptions[i].selected = i == closestIdx;
        mOptions[i].focusScale = .95 * mOptions[i].focusScale + .05 * (mOptions[i].selected ? 1.33 : 1.0);
    }

}

void SafariMap::draw() {
    ofClear(0);

    ofSetColor(255);
    mMapImage.draw(SCREEN_WIDTH * .15, SCREEN_HEIGHT * .1, SCREEN_WIDTH * .7, SCREEN_HEIGHT * .8);

    ofPushMatrix();
    ofTranslate(mGuidePos);
    ofScale(.8, .8);
    ofRotate(10 * sinf(mKietelWobbleAnimParam * 20) * (1 - mKietelWobbleAnimParam));
    ofTranslate(-mGuideImage.getWidth() / 2, -mGuideImage.getHeight() / 2);
    mGuideImage.draw(0,0);
    ofPopMatrix();


    for(int i=0; i<mOptions.size(); ++i) {
        ofPushMatrix();
        ofTranslate(mOptions[i].position);
        ofScale(.75, .75);
        ofScale(mOptions[i].focusScale, mOptions[i].focusScale);
        ofTranslate(-mOptions[i].image.getWidth()/2, -mOptions[i].image.getHeight()/2);
        mOptions[i].image.draw(0,0);
        ofPopMatrix();
    }

    ofPushMatrix();
    ofTranslate(mCursorPos);
    ofScale(.5, .5);
    ofTranslate(-mCursorImage.getWidth() * .2, -mCursorImage.getHeight() * .1);
    mCursorImage.draw(0,0);
    ofPopMatrix();
}

void SafariMap::kietel() {
    mKietelWobbleAnimParam = 0;
    mKietelTime = 1.0;
    mKietelSound.play();
}
