#pragma once

#include "Game.h"

class SafariGame : public Game {
public:
    SafariGame();
    virtual ~SafariGame();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();

private:
    ofSoundPlayer mCameraSoundId;
    ofVideoPlayer mVideo;

    ofImage mFrameImage;
    ofImage mViewImage;

    ofVec2f mCursorPos;

    ofFbo mPhotoBuffer;

    float mSnapshotTimeBuffer;
    float mSnapshotAnimParam;

    float mPhotoDropAnimParam;
    ofVec2f mPhotoDropLoc;
    float mPhotoAngleDeg;

    void makeSnapshot();
};