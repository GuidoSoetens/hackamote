#include "SafariGame.h"
#include "Defs.h"
#include "InputManager.h"

static const int PHOTO_WIDTH = SCREEN_WIDTH / 4;
static const int PHOTO_HEIGHT = SCREEN_HEIGHT / 4;

SafariGame::SafariGame() 
:   mCursorPos(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)
,   mSnapshotTimeBuffer(0)
,   mSnapshotAnimParam(1)
,   mPhotoDropAnimParam(1)
{

}

SafariGame::~SafariGame() {
    mCameraSoundId.unload();
}

void SafariGame::setup() {
    mCameraSoundId.load("safari/sound/camera.ogg");

    mFrameImage.loadImage("safari/img/frame.png");
    mViewImage.loadImage("safari/img/viewfinder.png");

    mVideo.load("safari/video/safari.mp4");
    mVideo.play();

    mPhotoBuffer.allocate(PHOTO_WIDTH, PHOTO_HEIGHT, GL_RGBA);
    mPhotoBuffer.begin();
    ofClear(0,0,0,255);
    mPhotoBuffer.end();
    // mPhotoBuffer.clear();
}

void SafariGame::makeSnapshot() {
    mSnapshotTimeBuffer = 1.0;
    mSnapshotAnimParam = 0.;
    mPhotoDropLoc = mCursorPos;
    mPhotoAngleDeg = (ofRandom(2.0) - 1.0) * 1.0;

    ofSetColor(255);
    mPhotoBuffer.begin();
    ofPushMatrix();
    ofTranslate(-mCursorPos + ofVec2f(PHOTO_WIDTH / 2, PHOTO_HEIGHT / 2));
    mVideo.draw(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    ofPopMatrix();
    mPhotoBuffer.end();

    mCameraSoundId.play();
}

void SafariGame::update(float dt) {

    mPhotoDropAnimParam = fminf(mSnapshotAnimParam + dt / 5.0, 1.0);
    mSnapshotTimeBuffer = fmaxf(mSnapshotTimeBuffer - dt, 0.0);
    mSnapshotAnimParam = fminf(mSnapshotAnimParam + dt, 1.0);
    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 1) {
        ofVec2f size(SCREEN_WIDTH, SCREEN_HEIGHT);
        ofVec2f center = size / 2.0;
        mCursorPos = center + 1.5 * size * wiimotes[0]->getPosition();
        mCursorPos.x = fmaxf(PHOTO_WIDTH/2, fminf(SCREEN_WIDTH - PHOTO_WIDTH/2, mCursorPos.x));
        mCursorPos.y = fmaxf(PHOTO_HEIGHT/2, fminf(SCREEN_HEIGHT - PHOTO_HEIGHT/2, mCursorPos.y));
        if((wiimotes[0]->isAButtonPressed() || wiimotes[0]->isBButtonPressed()) && mSnapshotTimeBuffer <= 0.0) {
            makeSnapshot();
        }
    }

    mVideo.update();
}

void SafariGame::draw() {
    ofSetColor(255);
    mVideo.draw(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    ofVec2f size(SCREEN_WIDTH, SCREEN_HEIGHT);
    ofVec2f center = size / 2.0;

    float scale = PHOTO_WIDTH / (float)mViewImage.getWidth();
    ofPushMatrix();
    ofTranslate(mCursorPos);
    ofScale(scale, scale);
    ofTranslate(-mViewImage.getWidth() / 2, -mViewImage.getHeight() / 2);
    mViewImage.draw(0,0);
    ofPopMatrix();

    if(mPhotoDropAnimParam < 1.0) {
        float t = 1 - cosf(powf(mPhotoDropAnimParam, 2.0) * .5 * M_PI);
        ofVec2f pos =  mPhotoDropLoc + t * ofVec2f(0, SCREEN_HEIGHT);
        float angle = mPhotoAngleDeg + t * 10;

        ofPushMatrix();
        ofTranslate(pos);
        ofRotate(angle);
        ofPushMatrix();
        ofScale(1.3, 1.4);
        ofScale(PHOTO_WIDTH / (float)mFrameImage.getWidth(), PHOTO_HEIGHT / (float)mFrameImage.getHeight());
        ofTranslate(-mFrameImage.getWidth() / 2, -mFrameImage.getHeight() / 2);
        mFrameImage.draw(0,0);
        ofPopMatrix();
        ofPushMatrix();
        ofTranslate(-mPhotoBuffer.getWidth() / 2, -mPhotoBuffer.getHeight() / 2);
        mPhotoBuffer.draw(0, 0);
        ofPopMatrix();
        ofPopMatrix();
    }

    ofSetColor(255, 255, 255, 255 * (1 - mSnapshotAnimParam));
    ofDrawRectangle(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
}