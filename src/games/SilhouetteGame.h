#pragma once

#include "Game.h"
#include "Speech.h"
#include "ofImage.h"
#include "json.h"

class SilhouetteGame : public Game {
public:
    SilhouetteGame();
    virtual ~SilhouetteGame();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();
private:

    typedef enum {
        GameState_Intro = 0,
        GameState_Guessing,
        GameState_RevealImage,
        GameState_ShowImage,
        GameState_FadeOut,
        GameState_Pause,
        GameState_Count
    } GameState;
    GameState mGameState;
    float mGameStateParameter;

    void setGameState(GameState state);

    typedef struct {
        string name;
        ofImage animalImage;
        ofImage shadowImage;
        ofSoundPlayer sound;
    } AnimalData;
    vector<AnimalData> mAnimalData;

    int mCurrentAnimalIndex;

    ofImage mBackgroundImage;


    Speech* mSpeech;
    vector<ofImage> mImages;

    Json::Value loadAnimalData();
};

