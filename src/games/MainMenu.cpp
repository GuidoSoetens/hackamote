#include "MainMenu.h"
#include "InputManager.h"

static const int NUM_OPTS = 6;

MainMenu::MainMenu(StartGameListener * gameListener) 
:   mCursorPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
,   mStartGameListener(gameListener)
,   mScrollTimer(0)
,   mScrollOffset(0)
,   mCenterIndex(0)
,   mSubsequentScrolls(0)
{
    
}

MainMenu::~MainMenu() {
    
}

void MainMenu::setup() {
    mCursorImage.loadImage("mainmenu/img/cursor.png");
    for(int i=0; i<NUM_OPTS; ++i) {
        mDoorImages.push_back(ofImage("mainmenu/img/door" + ofToString(i) + ".png"));
    }
}

void MainMenu::update(float dt) {
    auto wiimotes = InputManager::Instance()->getWiiMotes();
    if(wiimotes.size() >= 1) {
        ofVec2f size(SCREEN_WIDTH, SCREEN_HEIGHT);
        ofVec2f center = size / 2.0;
        mCursorPos = center + 1.5 * size * wiimotes[0]->getPosition();
        
        if(wiimotes[0]->isAButtonPressed() || wiimotes[0]->isBButtonPressed()) {
            if(mStartGameListener != NULL) {
                mStartGameListener->startGame(GameKey_SafariMap);
            }
        }
    }

    mScrollOffset *= fmaxf(1.0 - 5. * dt, 0.); 
    if(mCursorPos.x < SCREEN_WIDTH * .25 || mCursorPos.x > SCREEN_WIDTH * .75) {
        mScrollTimer += dt;
        if(mScrollTimer > .25) {
            int delta = (mCursorPos.x < SCREEN_WIDTH / 2) ? -1 : 1;
            mCenterIndex = (mCenterIndex + delta) % NUM_OPTS;
            if(mCenterIndex < 0)
                mCenterIndex += NUM_OPTS;

            mScrollOffset = delta;
            mScrollTimer = -1.0 + .85 * mSubsequentScrolls / 3.0;

            mSubsequentScrolls = min(mSubsequentScrolls + 1, 3);
        }
    }
    else {
        mScrollTimer = 0;
        mSubsequentScrolls = 0;
    }
}

void MainMenu::draw() {
    ofClear(0);

    //draw options:
    for(int i=0; i<5; ++i) {
        float frac_x = (i - 2 + mScrollOffset) / 2.0;

        int idx = (i - 2 + mCenterIndex) % NUM_OPTS;
        if(idx < 0)
            idx += NUM_OPTS;

        float scale = .7 - fabsf(frac_x) * .2;
        ofSetColor(255 * ofClamp(1 - fabsf(frac_x), 0, 1));
        ofPushMatrix();
        ofTranslate(SCREEN_WIDTH / 2 + SCREEN_WIDTH * frac_x * .6, SCREEN_HEIGHT / 2);
        ofScale(scale, scale);
        ofTranslate(-mDoorImages[idx].getWidth() / 2, -mDoorImages[idx].getHeight() / 2);
        mDoorImages[idx].draw(0,0);
        ofPopMatrix();
    }

    ofSetColor(255);
    ofPushMatrix();
    ofTranslate(mCursorPos);
    ofScale(.5, .5);
    ofTranslate(-mCursorImage.getWidth() * .2, -mCursorImage.getHeight() * .1);
    mCursorImage.draw(0,0);
    ofPopMatrix();
}