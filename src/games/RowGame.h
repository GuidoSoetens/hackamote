#pragma once

#include "Game.h"

class RowGame : public Game {
public:
    RowGame();
    virtual ~RowGame();

    virtual void setup();
    virtual void update(float dt);
    virtual void draw();

private:
    float mBoatTurnFactor;
    float mBoatSpeed;
    ofVec2f mBoatLocation;

    float mBoatAnimParam;

    float mRefreshTimer;

    typedef struct {
        ofVec2f position;
        ofImage* imageRef;
        float scale;
        bool skipReps;
        bool flipImage;
        float groundOffset;
    } FoliageData;
    vector<FoliageData> mFoliageElems;

    int mUnitsTraversed;

    ofSoundPlayer mBackgroundMusicId;
    vector<ofSoundPlayer> mPeddleSoundIds;
    ofSoundPlayer mRiverLoopId;

    float mSplashSoundTimer;

    ofImage mBackgroundImage;
    ofImage mBoatImage;
    ofImage mBushImage;
    ofImage mRockImage;
    ofImage mTreeImage;
    ofImage mWaterImage;

    ofShader mRiverShader;
};