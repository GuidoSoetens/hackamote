#include "SilhouetteGame.h"
#include "Defs.h"
#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;

SilhouetteGame::SilhouetteGame() 
    :   mSpeech( NULL )
    ,   mGameStateParameter(0)
{

}

SilhouetteGame::~SilhouetteGame() {
    delete mSpeech;
}

Json::Value SilhouetteGame::loadAnimalData() {\
    string str = "";
    ifstream t("./data/silhouettes/animals.json");
    if( t.is_open() ) {
        t.seekg(0, ios::end);
        str.reserve(t.tellg());
        t.seekg(0, ios::beg);
        str.assign((istreambuf_iterator<char>(t)),
                istreambuf_iterator<char>());
    }

    Json::Reader reader;
    Json::Value result;
    reader.parse(str, result);

    return result;
}

void SilhouetteGame::setup()
{
    mBackgroundImage.loadImage("silhouettes/img/background.png" );

    Json::Value root = loadAnimalData();
    vector<string> words;
    Json::Value animals = root["animals"];
    for(int i=0; i<animals.size(); ++i) {
        Json::Value animal = animals[i];
        string filename = animal["filename"].asString();
        string word = animal["word"].asString();
        words.push_back(word);
        AnimalData data = {
            word,
            ofImage("silhouettes/img/animals/" + filename + ".png"),
            ofImage("silhouettes/img/animals/" + filename + "_shadow.png")
        };
        data.sound.load("silhouettes/sound/" + filename + ".ogg");
        mAnimalData.push_back(data);
    }

    mSpeech = new Speech(words);

    std::random_shuffle(mAnimalData.begin(), mAnimalData.end());
    mCurrentAnimalIndex = 0;

    setGameState(GameState_Intro);
}

void SilhouetteGame::setGameState(GameState state) {

    mGameState = state;
    mGameStateParameter = 0;

    if(mGameState == GameState_Intro) {
        mCurrentAnimalIndex = (mCurrentAnimalIndex + 1) % mAnimalData.size();
    }
}

void SilhouetteGame::update(float dt)
{
    GameState nextGameState = (GameState)((mGameState + 1) % GameState_Count);
    bool regularUpdate = true;


    switch(mGameState) {
        case GameState_Guessing:
            {
                regularUpdate = false;
                auto word = mSpeech->update();
                if(word != NULL) {
                    string w = string(*word);
                    cout << mAnimalData[mCurrentAnimalIndex].name << " : " << w << endl;
                    if(w == mAnimalData[mCurrentAnimalIndex].name) {
                        setGameState(GameState_RevealImage);
                    }
                }
            }
            break;
        default:
            {
                //...
            }
    }

    if(regularUpdate) {
        mGameStateParameter += dt;
        if(mGameStateParameter > 1.0) {
            setGameState(nextGameState);
        }
    }
}

void SilhouetteGame::draw()
{
    ofSetColor(255);
    mBackgroundImage.draw(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);

    float shadowAlpha = 0.0;
    float animalAlpha = 0.0;

    switch(mGameState) {
        case GameState_Intro:
            shadowAlpha = sinf(mGameStateParameter * .5 * M_PI);
            break;
        case GameState_Guessing:
            shadowAlpha = 1.0;
            break;
        case GameState_RevealImage:
            shadowAlpha = cosf(mGameStateParameter * .5 * M_PI);
            animalAlpha = sinf(mGameStateParameter * .5 * M_PI);
            break;
        case GameState_ShowImage:
            animalAlpha = 1.0;
            break;
        case GameState_FadeOut:
            animalAlpha = cosf(mGameStateParameter * .5 * M_PI);
            break;
    }

    ofPushMatrix();
    ofTranslate(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    ofTranslate(-mAnimalData[mCurrentAnimalIndex].animalImage.getWidth() / 2, -mAnimalData[mCurrentAnimalIndex].animalImage.getHeight() / 2);
    float b = shadowAlpha * 255;
    ofSetColor(b, b, b, b);
    mAnimalData[mCurrentAnimalIndex].shadowImage.draw(0,0);
    b = animalAlpha * 255;
    ofSetColor(b, b, b, b);
    mAnimalData[mCurrentAnimalIndex].animalImage.draw(0,0);
    ofPopMatrix();
}

