#include "InputManager.h"

InputManager::~InputManager() {
    while(mWiiMotes.size() > 0) {
        delete mWiiMotes[0];
        mWiiMotes.erase(mWiiMotes.begin());
    }
}

InputManager* InputManager::Instance() {
    static InputManager instance = InputManager();
    if(!instance.mInitialized) {
        instance.init();
    }
    return &instance;
}

 void InputManager::init() {
    int numDevices = WiiMote::countAvaiableWiiMotes();
    for(int i=0; i<numDevices; ++i) {
        WiiMote * wiiMote = new WiiMote();
        wiiMote->setup(i);
        mWiiMotes.push_back(wiiMote);
    }
    mInitialized = true;
 }

 void InputManager::update(float dt) {
    for(int i=0; i<mWiiMotes.size(); ++i)
        mWiiMotes[i]->update(dt);
 }