#include "WiiMote.h"
#include "Defs.h"

WiiMote::WiiMote()
:   mPosition(0,0) 
,   mOrientation(0)
,   mAcceleration(0,0,0)
,   mRemoteIndex(0)
,   mMoveBuffer(0)
{

}

WiiMote::~WiiMote() {
   mWiiMote.rumble(false);
}

int WiiMote::countAvaiableWiiMotes() {
    return ofxXWiimote::listDevices();
}

string WiiMote::getDeviceStatus() {
    
    string res = "";
    res = "\nIDX: " + ofToString(mRemoteIndex) + "\n";
    res += "MOVE: " + ofToString(isMoving()) + "\n";
    res += "R|P|Y: " + ofToString((int)mAcceleration.x) + " " +  ofToString((int)mAcceleration.x) + " " + ofToString((int)mAcceleration.z) + "\n";
    res += "(X,Y): " + ofToString((int)mPosition.x) + ", " + ofToString((int)mPosition.y) + "\n";
    res += "Angle: " + ofToString((int)(180 * mOrientation / M_PI)) + "\n";

    return res;
}

void WiiMote::setup(int index) {
    mRemoteIndex = index;
    mWiiMote.setup(index + 1);
}

void WiiMote::update(float dt) {

    mWiiMote.getAccel(mAcceleration);
    mWiiMote.getSensorBarData(mPosition, mOrientation);

    bool moving = mAcceleration.length() > 150;
    if(moving) {
        mMoveBuffer = .5;
    }
    else {
        mMoveBuffer = fmaxf(0.0, mMoveBuffer - dt);
    }


    mRumbleTimer = fmaxf(0., mRumbleTimer - dt);
    mWiiMote.rumble(mRumbleTimer > 0.);

}

void WiiMote::rumble() {
    mRumbleTimer = .2;
}

void WiiMote::draw() {
    ofColor colors[4] = { ofColor(255,0,0), ofColor(0,0,255), ofColor(0,255,0), ofColor(255, 255, 0) };

    ofSetColor(colors[mRemoteIndex % 4]);

    ofVec2f screenCenter(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);

    ofPushMatrix();
    ofTranslate(screenCenter + SCREEN_WIDTH * mPosition);
    ofCircle(0,0,5);
    ofPopMatrix();
}