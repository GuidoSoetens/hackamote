#include "Speech.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <thread>
#include <stdlib.h>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

const string DIC_LOC = "/home/ies/julius_models/ENVR-v5.4.Dnn.Bin";
const string SPEECH_FILE = "/home/active-cues/spoken_word.txt";

auto Speech::update() -> const string*
{
    if(!boost::filesystem::exists(SPEECH_FILE))
        return nullptr;

    if(!_file.is_open()) {
        cout << "FILE NOT OPEN" << endl;
        return nullptr;
    }

    string word;
    while(std::getline(_file, word))
    {
        boost::algorithm::to_lower( word );
        cout << "Comparing " << word << " with the list..." <<endl;
        auto it = find (_word_list.begin(), _word_list.end(), word);
        if( it != _word_list.end() ) {
            _file.clear();
            return &(*it);
        }
    }
    _file.clear();
    return nullptr;
}


class Impl
{
    public:
    Impl();
    ~Impl();

    void run();
    auto start_daemon() -> void;
    auto stop_daemon() -> void;

    private:
    thread _t;
};

void Impl::run()
{
//    start_daemon();
//    using boost::asio::ip::tcp;
//
//    boost::asio::io_service io_service;
//    tcp::socket socket(io_service);
//
//    cout << "Try to connect." << endl;
//    socket.connect( tcp::endpoint( boost::asio::ip::address::from_string("localhost"), 10500));
//    cout << "Connected?" << endl;
//
//    const string msg { "Hello" };
//
//    boost::system::error_code error;
//    boost::asio::write( socket, boost::asio::buffer(msg), error);
//
//    if( error ) {
//        cout << "Help" << endl;
//        return;
//    }
//
//    boost::asio::streambuf receive_buffer;
//    boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
//
//    if( error && error != boost::asio::error::eof ) {
//        cout << "Help again." << endl;
//        return;
//    } else {
//        auto data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
//        cout << data << endl;
//    }
}

auto Impl::start_daemon() -> void {
    // Launch julius
    //string cmd = "cd " + DIC_LOC + "; julius -C mic.jconf -dnnconf dnn.conf -quiet -module &";
    //system(cmd.c_str());
}

auto Impl::stop_daemon() -> void {
    // Kill julius (naively)
    //string cmd = "pkill julius";
    //system(cmd.c_str());
}

Impl::Impl()
    : _t(&Impl::run, this)
{ }

Impl::~Impl() {
    _t.join();
}

Speech::Speech(const vector<string>& list)
    : _file( SPEECH_FILE )
    , _word_list(list)
    , p_impl(nullptr)
{
    ofstream ofs;
    ofs.open( SPEECH_FILE, std::ofstream::out | std::ofstream::trunc );
    ofs.close();
}

Speech::~Speech()
{
    _file.close();
}
