#pragma once

#include <string>
#include <vector>
#include <fstream>

class Impl;

class Speech {
    public:
        Speech(const std::vector<std::string>&);
        ~Speech();
        auto update() -> const std::string*;
    private:
        std::ifstream _file;
        std::vector<std::string> _word_list;
        Impl* p_impl;
};
