#pragma once 

#include "ofxXWiimote.h"

class ofxWiimoteExt : public ofxXWiimote {

    public:
        ofxWiimoteExt() { mHomePressed = false; };
        virtual ~ofxWiimoteExt() {};

        bool isHomePressed();

    protected:
        virtual void handle_keys(const struct xwii_event *event);

    private:
        bool mHomePressed;
};