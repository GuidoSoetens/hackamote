#include "ofMain.h"
#include "ofxWiimoteExt.h"

class WiiMote {
    public:
        WiiMote();
        virtual ~WiiMote();

        static int countAvaiableWiiMotes();
        void setup(int index);
        void update(float dt);
        void draw();

        string getDeviceStatus();
        ofVec2f getPosition() { return mPosition; };
        bool isMoving() { return mMoveBuffer > 0.0; };
        bool isAButtonPressed() { return mWiiMote.isAPressed(); };
        bool isBButtonPressed() { return mWiiMote.isBPressed(); };
        bool isHomeButtonPressed() { return mWiiMote.isHomePressed(); };

        void rumble();

    private:
        float mMoveBuffer;
        float mOrientation;
        ofVec2f mPosition;
        ofVec3f mAcceleration;

        float mRumbleTimer;

        int mRemoteIndex;
        ofxWiimoteExt mWiiMote;
};