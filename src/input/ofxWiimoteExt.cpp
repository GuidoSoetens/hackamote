#include "ofxWiimoteExt.h"

void ofxWiimoteExt::handle_keys(const struct xwii_event *event)
{
    ofxXWiimote::handle_keys(event);

    unsigned int code = event->v.key.code;
    bool pressed = event->v.key.state;

    if(code == XWII_KEY_HOME)
        mHomePressed = pressed;
}

bool ofxWiimoteExt::isHomePressed() {
    bool res = false;
    lock();
    res = mHomePressed;
    unlock();
    return res;
}