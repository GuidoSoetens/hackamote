#pragma once

#include "WiiMote.h"

class InputManager {
public:
    static InputManager* Instance();

    vector<WiiMote*> getWiiMotes() { return mWiiMotes; };
    void update(float dt);

private:
    InputManager(){};
    virtual ~InputManager();

    bool mInitialized;
    vector<WiiMote*> mWiiMotes;

    void init();
};